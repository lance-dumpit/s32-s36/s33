const jwt = require("jsonwebtoken");

const secret = "CourseBookingAPI";

// Token Creation 
module.exports.createAccessToken = (user) => {

	// When the user login, the token will be created with the user's information
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}

	// generate a JSON we token using the jwt's sign method
	// Generates the token using the form data and the secret code with no additional options provided.

	return jwt.sign(data, secret, {});
	
}