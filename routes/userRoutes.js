const express = require("express");
const router = express.Router();
const UserController = require("../controllers/UserControllers");

// Route for checking if email exists
router.post("/checkEmail", (req,res) => {
	UserController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));

})

// Route for User Registration
router.post("/register", (req,res) => {
	UserController.registerUser(req.body).then(resultFromController => res.send(
		resultFromController));

});


// Routes for User Authentication
router.post("/login", (req,res) => {

	UserController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
})

// Activity



// Route for Details
router.post("/details", (req,res) => {

	UserController.getProfile(req.body).then(resultFromController => res.send(resultFromController));

})



module.exports = router;